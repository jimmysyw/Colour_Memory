//
//  Card.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 5/12/2017.
//

import Foundation
import UIKit

enum CardStatus{
    case fold
    case shown
    case cleared
}
enum CardInfo: Int{
    case Card1_1 = 0
    case Card1_2 = 1
    case Card1_3 = 2
    case Card1_4 = 3
    case Card2_1 = 4
    case Card2_2 = 5
    case Card2_3 = 6
    case Card2_4 = 7
    case Card3_1 = 8
    case Card3_2 = 9
    case Card3_3 = 10
    case Card3_4 = 11
    case Card4_1 = 12
    case Card4_2 = 13
    case Card4_3 = 14
    case Card4_4 = 15
}
class Card{
    var cardStatus: CardStatus = .fold
    var cardInfo: CardInfo
    var cardMatchingIndex: Int = -1
    init(cardInfo: CardInfo){
        self.cardInfo = cardInfo
    }
    func isShown() -> Bool{
        return cardStatus == CardStatus.shown
    }
    func isFold() -> Bool{
        return cardStatus == CardStatus.fold
    }
    func isCleared() -> Bool{
        return cardStatus == CardStatus.cleared
    }
  
}
