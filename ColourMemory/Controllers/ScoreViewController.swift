//
//  ScoreViewController
//  ColourMemory
//
//  Created by Ying Wai Shum on 3/12/2017.
//

import UIKit

class ScoreViewCell: UITableViewCell{
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var scoreDataEntryView: UIStackView!
    
    func setupCell(){
        scoreDataEntryView.layer.borderWidth = 1
    }
}

class ScoreViewController: UITableViewController {
    
    var scoreViewModel: ScoreViewModel?
    var currentScoreRecord: ScoreRecordEntry?
    
    //Title View
    @IBOutlet weak var rankTitleLabel: UILabel!
    @IBOutlet weak var nameTitleLabel: UILabel!
    @IBOutlet weak var scoreTitleLabel: UILabel!
    @IBOutlet weak var rankTitleView: UIView!
    @IBOutlet weak var nameTitleView: UIView!
    @IBOutlet weak var scoreTitleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
       
    }
    
    func setupView(){
        scoreViewModel = ScoreViewModel()
        print("currentScoreRecord: \(currentScoreRecord)")
        if let scoreRecord = currentScoreRecord{
            scoreRecord.updateScoreRecordEntry()
        }
        
        scoreViewModel?.loadScoreRecords()
        scoreViewModel?.sortScoreRecordFromHighestScore()
        print("scoreViewModel.scoresRecords: \(scoreViewModel?.scoresRecords)")
        tableView.separatorStyle = .none
        setupTitleView()
    }
    
    func setupTitleView(){
        rankTitleView.layer.borderWidth = 1
        rankTitleView.layer.borderColor = UIColor.black.cgColor
        
        nameTitleView.layer.borderWidth = 1
        nameTitleView.layer.borderColor = UIColor.black.cgColor
        
        scoreTitleView.layer.borderWidth = 1
        scoreTitleView.layer.borderColor = UIColor.black.cgColor
        
    }
    

   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if let scoresRecords = scoreViewModel?.scoresRecords{
            if scoresRecords.count > 0{
                return 1
            }
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let scoreRecords = scoreViewModel?.scoresRecords{
            return scoreRecords.count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let scoreRecords = scoreViewModel?.scoresRecords{
            let scoreRecord = scoreRecords[indexPath.row]
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "scoreViewCell", for: indexPath) as? ScoreViewCell{
                
                cell.setupCell()
                cell.nameLabel.text = scoreRecord.name
                cell.scoreLabel.text = "\(scoreRecord.score)"
                
                if let rank = scoreRecord.rank{
                    cell.rankLabel.text = "\(rank)"
                }
                
                return cell
            }

            // Configure the cell...
           
          
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        return tableView.rowHeight
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    @IBAction func backToGameButtonDidClicked(_ sender: Any) {
        print("backToGameButtonDidClicked")
        
        self.performSegue(withIdentifier: "showGameVC", sender: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
