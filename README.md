# Colour Memory

## About
- "Colour Memory" is a simple memory game. The game board consists of a 4x4 grid with 8 pairs of color cards.
The player is to then flip two cards each round, trying to find a match. If the flipped pair is a match, the player receives 2 points. Otherwise, the cards are turned face down again and the player loses 1 point.

## Target Version
- iOS 9.0 or above

## Project Language
- Swift

## Framework used
- SwiftyBeaver - A Debugger friendly framework
- RealmSwift - A Swift version of Realm mobile internal database
- SwiftyTimer - A Timer framework for Swift iOS project

## Author
- Jimmy Shum