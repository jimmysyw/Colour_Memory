//
//  CardBoardViewController.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 5/12/2017.
//

import UIKit

class GameViewController: UIViewController {

    var gameViewModel: GameViewModel?
    
    //Header View
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var currentScoreLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var highScoreButton: UIButton!
  
    //Card Board View
    @IBOutlet weak var cardButton1_1: UIButton!
    @IBOutlet weak var cardButton1_2: UIButton!
    @IBOutlet weak var cardButton1_3: UIButton!
    @IBOutlet weak var cardButton1_4: UIButton!
    @IBOutlet weak var cardButton2_1: UIButton!
    @IBOutlet weak var cardButton2_2: UIButton!
    @IBOutlet weak var cardButton2_3: UIButton!
    @IBOutlet weak var cardButton2_4: UIButton!
    @IBOutlet weak var cardButton3_1: UIButton!
    @IBOutlet weak var cardButton3_2: UIButton!
    @IBOutlet weak var cardButton3_3: UIButton!
    @IBOutlet weak var cardButton3_4: UIButton!
    @IBOutlet weak var cardButton4_1: UIButton!
    @IBOutlet weak var cardButton4_2: UIButton!
    @IBOutlet weak var cardButton4_3: UIButton!
    @IBOutlet weak var cardButton4_4: UIButton!
    
    // Footer View
    @IBOutlet weak var restartButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameViewModel = GameViewModel()
        registerNotification()
        setupView()

        
        // Do any additional setup after loading the view.
    }
   
    deinit{
        unregisterNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView(){
        setupCardButton()
        setupLogo()
    }
    func setupCardButton(){
        cardButton1_1.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton1_2.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton1_3.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton1_4.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton2_1.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton2_2.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton2_3.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton2_4.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton3_1.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton3_2.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton3_3.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton3_4.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton4_1.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton4_2.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton4_3.setImage(UIImage(named: "card_bg"), for: .normal)
        cardButton4_4.setImage(UIImage(named: "card_bg"), for: .normal)
        
    }
    func setupLogo(){
        iconImageView.image = UIImage(named: "logo")
    }
    
    
    func registerNotification(){
        NotificationCenter.default.addObserver(forName: GameNotification.gameDataDidUpdate.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.updateGameView()
        })
    }
    func unregisterNotification(){
        NotificationCenter.default.removeObserver(self, name: GameNotification.gameDataDidUpdate.name, object: nil)
        
    }
    //MARK: - Clicking functions
    
    //Header View
    @IBAction func highScoreButtonDidClicked(_ sender: Any) {
        goToScoreView()
    }
    
    //Card Board View
    @IBAction func cb1_1DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card1_1)
    }
    @IBAction func cb1_2DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card1_2)
    }
    @IBAction func cb1_3DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card1_3)
    }
    @IBAction func cb1_4DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card1_4)
    }
    @IBAction func cb2_1DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card2_1)
    }
    @IBAction func cb2_2DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card2_2)
    }
    
    @IBAction func cb2_3DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card2_3)
    }
    @IBAction func cb2_4DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card2_4)
    }
    @IBAction func cb3_1DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card3_1)
    }
    @IBAction func cb3_2DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card3_2)
    }
    @IBAction func cb3_3DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card3_3)
    }
    @IBAction func cb3_4DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card3_4)
    }
    @IBAction func cb4_1DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card4_1)
    }
    @IBAction func cb4_2DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card4_2)
    }
    @IBAction func cb4_3DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card4_3)
    }
    @IBAction func cb4_4DidClicked(_ sender: Any) {
        gameViewModel?.updateCardAction(cardInfo: CardInfo.Card4_4)
    }
    
   
    //Footer View
    @IBAction func restartButtonDidClicked(_ sender: Any) {
        promptRestartMenu()
    }
    
    func updateGameView(){
        print("updateGameView")
        updateCardView()
        updateScoreView()
        
        if let isEndGame = gameViewModel?.isEndGame(){
            if isEndGame{
                prompUserInputMenu()
            }
        }
    }
    
    func updateCardView(){
        if let cards = gameViewModel?.game.cards{
            for card in cards{
                
                switch(card.cardInfo){
                case .Card1_1:
                    if card.cardStatus == .shown{
                        print("card: \(card)")
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton1_1.setImage(image, for: .normal)
                        }

                    }
                    else if card.cardStatus == .fold{
                        cardButton1_1.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton1_1.isHidden = true
                    }
                    
                   
                    break
                case .Card1_2:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton1_2.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton1_2.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton1_2.isHidden = true
                    }
                    break
                case .Card1_3:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton1_3.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton1_3.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton1_3.isHidden = true
                    }
                    break
                case .Card1_4:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton1_4.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton1_4.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton1_4.isHidden = true
                    }
                    break
                case .Card2_1:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton2_1.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton2_1.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton2_1.isHidden = true
                    }
                    break
                case .Card2_2:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton2_2.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton2_2.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton2_2.isHidden = true
                    }
                    break
                case .Card2_3:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton2_3.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton2_3.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton2_3.isHidden = true
                    }
                    break
                case .Card2_4:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton2_4.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton2_4.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton2_4.isHidden = true
                    }
                    break
                case .Card3_1:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton3_1.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton3_1.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton3_1.isHidden = true
                    }
                    break
                case .Card3_2:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton3_2.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton3_2.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton3_2.isHidden = true
                    }
                    break
                case .Card3_3:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton3_3.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton3_3.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton3_3.isHidden = true
                    }
                    break
                case .Card3_4:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton3_4.setImage(image, for: .normal)
                        }
                        
                    }
                    else if card.cardStatus == .fold{
                        cardButton3_4.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton3_4.isHidden = true
                    }
                    break
                case .Card4_1:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton4_1.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton4_1.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton4_1.isHidden = true
                    }
                    break
                case .Card4_2:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton4_2.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton4_2.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton4_2.isHidden = true
                    }
                    break
                case .Card4_3:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton4_3.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton4_3.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton4_3.isHidden = true
                    }
                    break
                case .Card4_4:
                    if card.cardStatus == .shown{
                        if let image = getCardImageByIndex(index: card.cardMatchingIndex){
                            cardButton4_4.setImage(image, for: .normal)
                        }
                    }
                    else if card.cardStatus == .fold{
                        cardButton4_4.setImage(UIImage(named: "card_bg"), for: .normal)
                    }
                    else{
                        cardButton4_4.isHidden = true
                    }
                    break
                    
                }
                
            }
            
        }
        
    }
    
    func getCardImageByIndex(index: Int) -> UIImage?{
        switch(index){
        case 0: return UIImage(named: "colour1")
        case 1: return UIImage(named: "colour2")
        case 2: return UIImage(named: "colour3")
        case 3: return UIImage(named: "colour4")
        case 4: return UIImage(named: "colour5")
        case 5: return UIImage(named: "colour6")
        case 6: return UIImage(named: "colour7")
        case 7: return UIImage(named: "colour8")
        default: return nil
        }
    }
    
    func updateScoreView(){
        if let point = gameViewModel?.game.points{
            scoreLabel.text = "\(point)"
            //
            
        }
        
    }
    
    func goToScoreView(){
        self.performSegue(withIdentifier: "showScoreVC", sender: self)
    }
    
    func promptRestartMenu(){
        
        // create the alert
        let alert = UIAlertController(title: "Restart Game", message: "Would you like to restart the game?", preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: restartGame))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func restartGame(alert: UIAlertAction!) {
        gameViewModel?.restartGame()
    }
    
    func prompUserInputMenu(){
        
        let alert = UIAlertController(title: "Game Finish!", message: "Please enter your name.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Your Name"
            textField.addTarget(alert, action: #selector(alert.textDidChangeInUserAlert), for: .editingChanged)
        })
        
    
        let userInputAction = UIAlertAction(title: "OK", style: .default) { [unowned self] _ in
            guard let user = alert.textFields?[0].text
            else { return } // Should never happen
            
            //perform show score action
            if let score = self.gameViewModel?.game.points{
                self.gameViewModel?.saveScoreRecordForCurrentGame(player: user, score: score)
            }
            
            self.performSegue(withIdentifier: "showScoreVC", sender: self)
        }
        
        userInputAction.isEnabled = false
        alert.addAction(userInputAction)
     
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showScoreVC" {
            let scoreVC = segue.destination as! ScoreViewController
            if let currentScoreRecord = gameViewModel?.currentScoreRecord{
                scoreVC.currentScoreRecord = currentScoreRecord
            }
            
             //wdsvc.workDayContainerView.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    

}
