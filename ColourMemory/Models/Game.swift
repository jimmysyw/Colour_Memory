//
//  Game.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 7/12/2017.
//

import Foundation

enum GameStatus{
    case pending
    case running
    case finish
}
enum CardMatchStatus{
    case success
    case failure
}

class Game{
    var gameStatus: GameStatus = .pending
    var cards: [Card] = []
    var points: Int = 0
    
    init(){
        initGameSetting()
    }
    
    
    func initCards(){
        cards = []
        cards.append(Card.init(cardInfo: CardInfo.Card1_1))
        cards.append(Card.init(cardInfo: CardInfo.Card1_2))
        cards.append(Card.init(cardInfo: CardInfo.Card1_3))
        cards.append(Card.init(cardInfo: CardInfo.Card1_4))
        cards.append(Card.init(cardInfo: CardInfo.Card2_1))
        cards.append(Card.init(cardInfo: CardInfo.Card2_2))
        cards.append(Card.init(cardInfo: CardInfo.Card2_3))
        cards.append(Card.init(cardInfo: CardInfo.Card2_4))
        cards.append(Card.init(cardInfo: CardInfo.Card3_1))
        cards.append(Card.init(cardInfo: CardInfo.Card3_2))
        cards.append(Card.init(cardInfo: CardInfo.Card3_3))
        cards.append(Card.init(cardInfo: CardInfo.Card3_4))
        cards.append(Card.init(cardInfo: CardInfo.Card4_1))
        cards.append(Card.init(cardInfo: CardInfo.Card4_2))
        cards.append(Card.init(cardInfo: CardInfo.Card4_3))
        cards.append(Card.init(cardInfo: CardInfo.Card4_4))
    }
    
    func initGameSetting(){
        gameStatus = .pending
        initCards()
        points = 0
    }
    
    func startGame(){
        initGameSetting()
        shuffleCards()
        gameStatus = .running
    }
    
    func endGame(){
        gameStatus = .finish
        
    }
    
    
    func shownCard(card: Card){
        card.cardStatus = .shown
        cards[card.cardInfo.rawValue] = card
    }
    func foldCard(card: Card){
        card.cardStatus = .fold
        cards[card.cardInfo.rawValue] = card
    }
    
    func clearCard(card: Card){
        card.cardStatus = .cleared
        cards[card.cardInfo.rawValue] = card
    }
    
    func shuffleCards(){
        let numList = genRandomNumList(size: cards.count)
        let matchingNumList = convertNumListToMatchingNumList(numList: numList)
        
        for i in 0...matchingNumList.count-1{
            cards[i].cardMatchingIndex = matchingNumList[i]
            print("card: \(cards[i])")
        }
        
    }
    
    func genRandomNumList(size: Int) -> [Int]{
        var numList: [Int] = []
        while numList.count != size{
            let num = Int(arc4random_uniform(UInt32(size)))
            if !numList.contains(num){
                numList.append(num)
            }
        }
        return numList
    }
    func convertNumListToMatchingNumList(numList: [Int]) -> [Int]{
        var matchingNumList: [Int] = []
        for num in numList{
            if num >= numList.count/2{
                matchingNumList.append(num - numList.count/2)
            }
            else{
                matchingNumList.append(num)
            }
        }
        return matchingNumList
        
    }
    func updatePoint(cardMatchResult: CardMatchStatus){
        switch(cardMatchResult){
        case .success:
            points = points + 2
            break
        case .failure:
//            if points > 1{
//                points = points - 1
//            }
            points = points - 1
            break
        default:
            break
        }
        
    }
    
    
}
