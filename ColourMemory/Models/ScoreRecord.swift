//
//  ScoreRecord.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 6/12/2017.
//

import UIKit
import Foundation
import RealmSwift

class ScoreRecordRealmEntry: Object{
    @objc dynamic var scoreRecordId = -1
    @objc dynamic var score: Int = 0
    @objc dynamic var name: String = ""
    override static func primaryKey() -> String{
        return "scoreRecordId"
    }
    
    convenience init(score: Int, name: String){
        self.init()
        self.score = score
        self.name = name
    }
    
}

class ScoreRecordEntry{
    var scoreRecordId: Int = -1
    var score: Int = -1
    var name: String = ""
    var rank: Int?
    static let scoreRecordEntryDAO = RealmBaseDao<ScoreRecordRealmEntry>()
    
    init(scoreRecordEntry: ScoreRecordRealmEntry){
        score = scoreRecordEntry.score
        name = scoreRecordEntry.name
        
    }
    static func loadAllScoreRecordEntries() -> [ScoreRecordEntry]{
        let objects = scoreRecordEntryDAO.findAll()
        return objects.map {ScoreRecordEntry(scoreRecordEntry: $0)}
        
    }
    static func loadScoreRecordEntryById(id: Int) -> ScoreRecordEntry?{
        guard let object = scoreRecordEntryDAO.findFirst(key: id as AnyObject) else { return nil }
        return ScoreRecordEntry(scoreRecordEntry: object)
        
    }
    static func createScoreRecordEntry() -> ScoreRecordEntry {
        let object = ScoreRecordRealmEntry()
        object.scoreRecordId = -1
        object.score = 0
        object.name = ""
        
        let viewModel = ScoreRecordEntry(scoreRecordEntry: object)
        return viewModel
    }
    
    func updateScoreRecordEntry() {
        let dao = type(of: self).scoreRecordEntryDAO
    
        let object = ScoreRecordRealmEntry()
        object.score = score
        object.name = name
      
        if scoreRecordId == -1{
            object.scoreRecordId = ScoreRecordEntry.scoreRecordEntryDAO.newId()!
            ScoreRecordEntry.scoreRecordEntryDAO.add(d: object)
        }
        else{
            object.scoreRecordId = scoreRecordId
            let _ = dao.update(d: object)
        }
        
    }
    
    func deleteRecordEntry() {
        let dao = type(of: self).scoreRecordEntryDAO
        guard let object = dao.findFirst(key: scoreRecordId as AnyObject) else {
            return
        }
        dao.delete(d: object)
    }
}
