//
//  ScoreViewModel.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 3/12/2017.
//

import Foundation
import UIKit


class ScoreViewModel{
    var scoresRecords: [ScoreRecordEntry] = []
    
    init(){
        loadScoreRecords()
    }
    
    //get the score records from realm database
    func loadScoreRecords(){
        scoresRecords = ScoreRecordEntry.loadAllScoreRecordEntries()
        if scoresRecords.count > 0{
            for i in 0...scoresRecords.count-1{
                let record = scoresRecords[i]
                record.rank = i+1
                scoresRecords[i] = record
            }
            addRankToScoreRecord(scoreRecords: scoresRecords)
        }
    }
    func sortScoreRecordFromHighestScore(){
        scoresRecords = scoresRecords.sorted(by: { $0.score > $1.score })
        addRankToScoreRecord(scoreRecords: scoresRecords)
    }
    
    func addRankToScoreRecord(scoreRecords: [ScoreRecordEntry]){
        if scoresRecords.count > 0{
            for i in 0...scoresRecords.count-1{
                let record = scoresRecords[i]
                record.rank = i+1
                scoresRecords[i] = record
            }
        }
    }
    
    
}

