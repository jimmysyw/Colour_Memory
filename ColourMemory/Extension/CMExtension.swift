//
//  CMExtension.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 3/12/2017.
//

import Foundation
import UIKit


protocol NotificationName {
    var name: Notification.Name { get }
}

extension RawRepresentable where RawValue == String, Self: NotificationName {
    var name: Notification.Name {
        get {
            return Notification.Name(self.rawValue)
        }
    }
}

extension UIAlertController {
    
    func isValidUser(_ user: String) -> Bool{
        return user.characters.count > 0
    }
    
    @objc func textDidChangeInUserAlert() {
        if let user = textFields?[0].text,
            let action = actions.last {
            action.isEnabled = isValidUser(user)
        }
    }
}

class CMExtension{
    
}
