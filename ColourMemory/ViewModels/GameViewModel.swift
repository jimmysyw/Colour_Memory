//
//  CardBoardViewModel.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 5/12/2017.
//

import Foundation
import UIKit
import SwiftyTimer

enum GameNotification: String, NotificationName{
    case gameDataDidUpdate
}

class GameViewModel{
    var game: Game
    var previousSelectedCard: Card?
    var viewModelDidUpdate: (() -> Void)?
    var isProcessing: Bool = false
    var currentScoreRecord: ScoreRecordEntry?
    init(){
        game = Game()
        startGame()
    }
    
    
    func startGame(){
        game.startGame()
    
    }
    
   

    
    func restartGame(){
        game.startGame()
        NotificationCenter.default.post(name:GameNotification.gameDataDidUpdate.name, object: nil)
    }
    func updateCardAction(cardInfo: CardInfo){
        let card = game.cards[cardInfo.rawValue]
        if card.isFold(){
            if !isProcessing{
                card.cardStatus = CardStatus.shown
                if previousSelectedCard != nil{
                   isProcessing = true
                    let timer = Timer.after(1.seconds){
                        DispatchQueue.main.async {
                            self.isProcessing = false
                            self.checkSelectedCard(card: card)
                        }
                    }
                    timer.start()
                    
                    
                }
                else{
                    previousSelectedCard = card
                }
                
                NotificationCenter.default.post(name: GameNotification.gameDataDidUpdate.name, object: nil)
            }
        }
    }
    
    
    
    func checkSelectedCard(card: Card){
        if let previousCard = previousSelectedCard{
            if isMatchingCards(card1: previousCard, card2: card){
                print("Match!")
                game.updatePoint(cardMatchResult: CardMatchStatus.success)
                game.clearCard(card: previousCard)
                game.clearCard(card: card)
                
                
            }
                
            else{
                game.updatePoint(cardMatchResult: CardMatchStatus.failure)
                game.foldCard(card: previousCard)
                game.foldCard(card: card)
                print("Not Match...")
            }
            
            
            previousSelectedCard = nil
        }
        
        NotificationCenter.default.post(name: GameNotification.gameDataDidUpdate.name, object: nil)
    }
    

    func isMatchingCards(card1: Card, card2: Card) -> Bool{
        return card1.cardMatchingIndex == card2.cardMatchingIndex
    }
    
    func isEndGame() -> Bool{
        for card in game.cards{
            if card.cardStatus != CardStatus.cleared{
                return false
            }
            
        }
        return true
   
    }
    func saveScoreRecordForCurrentGame(player: String,score: Int){
        currentScoreRecord = createScoreRecordForCurrentGame(player: player,score: score)
    }
    func createScoreRecordForCurrentGame(player: String,score: Int) -> ScoreRecordEntry{
        let scoreRecord = ScoreRecordEntry.createScoreRecordEntry()
        scoreRecord.name = player
        scoreRecord.score = score
        return scoreRecord
    }
    
    
}
