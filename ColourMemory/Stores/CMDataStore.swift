//
//  CMDataStore.swift
//  ColourMemory
//
//  Created by Ying Wai Shum on 3/12/2017.
//

import UIKit
import RealmSwift

class CMDataStore{
    static let sharedInstance = CMDataStore()
    static let scoreFolderName = "Score"
    static let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    
    let defaultFileManager = FileManager.default
    
    init(){
        readyFolderIfNeeded()
    }
    func readyFolderIfNeeded() {
        var isDir : ObjCBool = true
        
        logger.debug("score folder path = \(scoreRecordFolderPath())")
        if !defaultFileManager.fileExists(atPath: scoreRecordFolderPath(), isDirectory: &isDir) {
            do {
                try defaultFileManager.createDirectory(atPath: scoreRecordFolderPath(), withIntermediateDirectories: true, attributes: nil)
            }
            catch {
                logger.error("fail to create score folder! : \(error)")
            }
        }
        
    }
    
    func scoreRecordFolderPath() -> String{
         return CMDataStore.documentDirectory.appending("/\(CMDataStore.scoreFolderName)")
    }
    
}
